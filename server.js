var app = require('express')();
var server = require('http').Server(app);
var every = require('schedule').every;
var fs = require('fs');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));
io = require('socket.io')(server);

var usernames = [];
var rooms = [];

io.sockets.on('connection', function(socket){
  every('2s').do(function(){
    fs.readFile(__dirname + '/q.json', 'utf-8', function(err, result){
    if (err) console.log(err);
      socket.emit('dataGrid', result);
    });
  });

  socket.on('addUser', function(username){
    if (rooms.indexOf(username) < 0 ) {
      socket.join(username);
      rooms.push(username);
      socket.username = username;
      usernames.push(username);

      socket.in(username).on('disconnect', function(){
        usernames.splice(username);
      });

      socket.in(username).on('sendChat', function(data){
        var pattern = new RegExp(username);
        var hasil = pattern.exec(data.nama);
        var nama;
        if (hasil) {
          nama = "Anda";
        }else{
           nama = data.nama;
        }
        var dataChat = "<b>"+nama+"</b> : "+data.pesan+" <sub>"+data.tgl+"</sub>";
        io.sockets.to(username).emit('dataChat', dataChat);
      });
    }
  });

  socket.on('tab3', function(){
    io.sockets.emit('dataUser', usernames);
  });

});

app.get('/', function(req,res) {
  res.send(rooms);
});

server.listen(8090);
console.log('app running at port 8090');
