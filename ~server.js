var app = require('express')();
var server = require('http').Server(app);
var every = require('schedule').every;
var fs = require('fs');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));
io = require('socket.io')(server);

pilihUserChat = [];
io.sockets.on('connection', function(socket){
  var ipAddress = socket.handshake.address;

  every('2s').do(function(){
    fs.readFile(__dirname + '/q.json', 'utf-8', function(err, result){
    if (err) console.log(err);
      socket.emit('dataGrid', result);
      // fs.appendFile('chat.txt', result, function(){});
    });
  });

  socket.on('join', function(room){
    if (pilihUserChat.indexOf(room) < 0 ) {
      socket.join(room);
      pilihUserChat.push(room);

        socket.in(room).on('sendChat', function(data){
            io.sockets.in(room).emit('dataChat', data);
            // console.log(data);
        });

    }
  });
    socket.on('adminJoinChatRoom', function(room){
      socket.join(room);
        socket.in(room).on('sendChat', function(data){
            io.sockets.in(room).emit('dataChat', data);
        });
    });

  socket.on('tab4', function(){
    io.sockets.emit('dataTab4', pilihUserChat);
  });

});

app.get('/', function(req,res) {
  res.send(pilihUserChat);
});

server.listen(8090);
console.log('app running at port 8090');
